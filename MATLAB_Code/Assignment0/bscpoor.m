% Start time measurement
tic();

%% Source: Generate random bits

nb_bits=1000000;
txbits = randi([0 1],nb_bits,1);


%% Mapping: Bits to symbols

tx ='A'.*not(txbits) + 'B'.*txbits; % 'A' for a bit O and 'B' for a bit 1

%% Channel: Apply BSC

randval = rand(nb_bits,1);

index_change_A=(randval<0.2) & (tx=='A'); % matrix with 1 at position where A will switch to B 
index_change_B=(randval<0.2) & (tx=='B'); % matrix with 1 at position where B will switch to A 
index_no_change = (randval>0.2); % matrix with 1 at no changement
 
rx='B'.*index_change_A + 'A'.*index_change_B + tx.*index_no_change ; % implementation of channel changement


%% Demapping: Symbols to bits

rxbits = (rx== 'B') ; % demapping


%% BER: Count errors

errors = sum(rxbits~=txbits); % sum of all difference between data transmit and data received


%% Output result

disp(['BER: ' num2str(errors/nb_bits*100) '%'])

% Stop time measurement
toc()
