load('task2.mat', 'image_size', 'signal'); % load variable 'image_size' and 'signal'
num_bit_img = image_size(1)* image_size(2)*4;
SNR = 10;
signal_awgn = signal + sqrt( 1 / 10^(SNR/10) /2) * (randn(size(signal))+1j*randn(size(signal)));

preamble = preamble_gen_solution(100);
preamble_bpsk = -2*(preamble) + 1;

[start] = detector_solution(preamble_bpsk, signal_awgn, 30);
payload = signal_awgn(start: start+num_bit_img-1, 1);
image = image_decoder(demapper(payload), image_size);