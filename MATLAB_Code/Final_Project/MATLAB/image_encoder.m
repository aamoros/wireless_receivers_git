function [bit_stream,image_size] = image_encoder(image_name)
%Convert image to bit stream data
%   Detailed explanation goes here

RGB = imread(image_name);

I = rgb2gray(RGB);

image_size = size(I);

I_dec = de2bi(I);

bit_stream=zeros(8*prod(image_size),1);
idx =1 ;
for i=1:prod(image_size)
    for ii=1:8 
        bit_stream(idx)=I_dec(i,ii);
        idx=idx+1;
    end
end

end

