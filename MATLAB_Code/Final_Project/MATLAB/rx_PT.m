function [rxbits] = rx_PT(rxsignal,conf)
% Digital Receiver
%
%   [rxsignal conf] = rx(txbits,conf,k) implements a complete causal
%   receiver in digital domain.
%
%   rxsignal    : received signal
%   conf        : configuration structure
%   k           : frame index
%
%   Outputs
%
%   rxbits      : received bits
%   conf        : configuration structure
%

% LP down conversion 

% f=conf.f_s*(0:length(rxsignal)-1)/length(rxsignal);

time = 0:1/conf.f_s:length(rxsignal)*(1/conf.f_s)-(1/conf.f_s);
LP_down_signal = 2*lowpass(transpose(rxsignal).*exp(-2i*pi*conf.f_c.*time),conf);
% 
f=conf.f_s*(0:length(LP_down_signal)-1)/length(LP_down_signal);
% 
figure(5)
plot(f,fft(LP_down_signal));
title("Signal received in frequency domain");
% xlim([0 1000]);
% ylim([-2 2].*1e4);
xlabel("Frequency(Hz)");
ylabel("FFT Amplitude");
grid on;

    
[data_idx, theta] = frame_sync(matched_filter(LP_down_signal,conf.npreamble, conf.filter_length),conf.os_factor_preamble);

%data_idx = 48000;

N=conf.nsubfreq*conf.os_factor+conf.cp_length;   % number of symbol sent on each subcarrier
num_cols_rx=floor(conf.nsyms/(conf.nsubfreq-conf.nb_pilots));% remove last OFDM symbol for simplicity
%num_cols_rx = ceil((conf.nsyms+(conf.nb_pilots * (conf.nsyms/conf.nsubfreq)))/conf.nsubfreq)+2; %remove last OFDM symbol for simplicity
length_payload = N*num_cols_rx ; % number of payload symbols
rx_payload = LP_down_signal(data_idx:data_idx+length_payload-1);


%Serial to parallel change
S2P_rxsignal = reshape(rx_payload,N,[]) ;


% CP prefixe
S2P_no_cp = S2P_rxsignal(conf.cp_length+1:end,:);

% fft and downsampling
S2P_fft=zeros(conf.nsubfreq,num_cols_rx); % Take new oversampling size into account and training symbol
for i =1:num_cols_rx
    S2P_fft(:,i) = osfft(S2P_no_cp(:,i),conf.os_factor) ;
end

% Training symbol generation 

training_symbol_rx = 1-2*lfsr_framesync(conf.nb_pilots);

% Training symbol pilot comparaison

S2P_rxfreq=zeros(conf.nsubfreq-conf.nb_pilots,num_cols_rx); % Rx data without pilots in frequency domain
H_f=zeros(conf.nsubfreq-conf.nb_pilots,num_cols_rx);
h=zeros(conf.nsubfreq-conf.nb_pilots,num_cols_rx);
PDP=zeros(conf.nsubfreq-conf.nb_pilots,num_cols_rx);
idx_start = 1;
idx_data = 1;
idx_train = 1;

for i = 1:num_cols_rx
     y = zeros(conf.nb_pilots,1);
     x = zeros(conf.nb_pilots,1);
    for ii = 1:conf.nsubfreq
        if mod(ii,conf.step_training) ~= idx_start
                S2P_rxfreq(idx_data,i)=S2P_fft(ii,i);
                idx_data = idx_data + 1;
        elseif mod(ii,conf.step_training) == idx_start
            y(idx_train)= S2P_fft(ii,i);
            x(idx_train)=ii;
            idx_train = idx_train + 1;
        end
    end
   
    idx_train = 1; % restart training frame
    idx_data = 1 ; % restart index data
    
    idx_start = idx_start + 1;
    if idx_start == (conf.step_training)
        idx_start = 1 ;
    end
    
    %interpolation Chennel estimation
    xq=1:conf.nsubfreq-(conf.nb_pilots);
    y=y./training_symbol_rx; % Comparison with expected training pilots
    H_f(:,i)=interp1(x,y,xq,'spline').'; % Cubic interpolation
    

    % Equalization 
     S2P_rxfreq(:,i) = (S2P_rxfreq(:,i).*exp(-1i*angle(H_f(:,i))))./abs(H_f(:,i));
     
     %Channel impulse response
    h(:,i)=ifft(H_f(:,i));

    %Power Delay profile
    PDP(:,i) = abs(h(:,i)).^2;
    
end



%f_6=1:conf.f_spacing:conf.f_spacing*(conf.nsubfreq-conf.nb_pilots);

for i = 1:ceil(num_cols_rx/10)
    
    figure(16)
    hold on;
    plot(abs(H_f(:,i)));
    % xlim([0 256]);
    ylim([0 500]);
    title('Channel estimator magnitude in frequency domain');
    xlabel('Subcarriers');
    ylabel('$\hat{H}(f)$ Magnitude', 'interpreter','latex');
    grid on;

    figure(17)
    hold on;
    plot(angle(H_f(:,i)));
    title('Channel estimator angle in frequency domain');
    xlabel('Subcarriers');
    ylabel('$\hat{H}(f)$ Angle', 'interpreter','latex');
    grid on;
    
    figure(18)
    hold on;
    plot(PDP(:,i));
    title('Power Delay Profile');
    xlabel('Time axis');
    ylabel('PDP');
    grid on;

end
%Parralel to serie change
rxsignal = transpose(S2P_rxfreq(:));


figure(7);
plot(real(rxsignal),imag(rxsignal),'r*');
title('Modulated OFDM received signal extracted');
xlabel('Real axis')
ylabel('Imaginary axis')
grid on;

rxbits=demapper(rxsignal);