close all
% % % % %
% Wireless Receivers: algorithms and architectures
% Audio Transmission Framework 
%
%
%   3 operating modes:
%   - 'matlab' : generic MATLAB audio routines (unreliable under Linux)
%   - 'native' : OS native audio system
%       - ALSA audio tools, most Linux distrubtions
%       - builtin WAV tools on Windows 
%   - 'bypass' : no audio transmission, takes txsignal as received signal

% Configuration Values
conf.audiosystem        = 'bypass'; % Values: 'matlab','native','bypass'
conf.training = 'PT'; % one = one symbol training / 'PT' = phase tracking
conf.mode = 'random' ; % Values: 'random','image'

conf.f_s                = 48000;   % sampling rate  
conf.f_spacing          = 5;   % frequency spacing
conf.f_symsp            = 100 ;     % symbol rate preamble
conf.nframes            = 1;       % number of frames to transmit
conf.nbits              = 51200 %295424 ;    % number of bits 
conf.modulation_order   = 2; % BPSK:1, QPSK:2
conf.f_c                = 8000 ; % frequence carrier
conf.nsubfreq           = 256; % number of OFDM frequency subcarriers
conf.filter_length      = 12;

conf.npreamble          = 100;
conf.bitsps             = 16;   % bits per audio sample
% conf.offset             = 0;

% Init Section
% all calculations that you only have to do once
conf.os_factor  = conf.f_s/(conf.f_spacing*conf.nsubfreq);
conf.os_factor_preamble  = conf.f_s/conf.f_symsp;

if mod(conf.os_factor_preamble,1) ~= 0
   disp('WARNING: Sampling rate must be a multiple of the symbol rate'); 
end
conf.nsyms     = ceil(conf.nbits/conf.modulation_order);
conf.nsymsub   = conf.nsyms/conf.nsubfreq;

conf.cp_length = (conf.nsubfreq*conf.os_factor)/2; % length CP prefixe

conf.preamble_filter_length = 10 ; % We would like a 20 symbols period filter length

conf.step_training = 8 ; % Values : 8 / 16 / 32 / 64 / 128 for step between training pilots

conf.nb_pilots = conf.nsubfreq/conf.step_training; %number of pilots by OFDM symbol

% Initialize result structure with zero
res.biterrors   = zeros(conf.nframes,1);
res.rxnbits     = zeros(conf.nframes,1);

% TODO: To speed up your simulation pregenerate data you can reuse
% beforehand.


% Results


for k=1:conf.nframes
    
    % Generate random data
    
    if strcmp(conf.mode,'random')
        txbits = randi([0 1],conf.nbits,1);
        l_tx=length(txbits);
    elseif strcmp(conf.mode,'image')
        [txbits,image_size]=image_encoder('photo-toulouse.jpg');
        l_tx=length(txbits);
        if mod(l_tx/2,conf.nsubfreq)~=0
            nb_zero=2*( 256 - mod(l_tx/2,conf.nsubfreq));
            txbits=[txbits ;zeros(nb_zero,1)];
        end
    end
    
    % TODO: Implement tx() Transmit Function
    if strcmp(conf.training,'one')
        [txsignal] = tx(txbits,conf);
    elseif strcmp(conf.training,'PT')
        [txsignal ] = tx_PT(txbits,conf);
    end
    
    % % % % % % % % % % % %
    % Begin
    % Audio Transmission
    %
    
    % normalize values
    peakvalue       = max(abs(txsignal));
    normtxsignal    = txsignal / (peakvalue + 0.3);
    
    % create vector for transmission
    rawtxsignal = [ zeros(conf.f_s,1) ; normtxsignal ;  zeros(conf.f_s,1) ]; % add padding before and after the signal
    rawtxsignal = [  rawtxsignal  zeros(size(rawtxsignal)) ]; % add second channel: no signal
    txdur       = length(rawtxsignal)/conf.f_s; % calculate length of transmitted signal
    
%     wavwrite(rawtxsignal,conf.f_s,16,'out.wav')   
    audiowrite('out.wav',rawtxsignal,conf.f_s)  
    
    % Platform native audio mode 
    if strcmp(conf.audiosystem,'native')
        
        % Windows WAV mode 
        if ispc()
            disp('Windows WAV');
            wavplay(rawtxsignal,conf.f_s,'async');
            disp('Recording in Progress');
            rawrxsignal = wavrecord((txdur+1)*conf.f_s,conf.f_s);
            disp('Recording complete')
            rxsignal = rawrxsignal(1:end,1);

        % ALSA WAV mode 
        elseif isunix()
            disp('Linux ALSA');
            cmd = sprintf('arecord -c 2 -r %d -f s16_le  -d %d in.wav &',conf.f_s,ceil(txdur)+1);
            system(cmd); 
            disp('Recording in Progress');
            system('aplay  out.wav')
            pause(2);
            disp('Recording complete')
            rawrxsignal = wavread('in.wav');
            rxsignal    = rawrxsignal(1:end,1);
        end
        
    % MATLAB audio mode
    elseif strcmp(conf.audiosystem,'matlab')
        disp('MATLAB generic');
        playobj = audioplayer(rawtxsignal,conf.f_s,conf.bitsps);
        recobj  = audiorecorder(conf.f_s,conf.bitsps,1);
        record(recobj);
        disp('Recording in Progress');
        playblocking(playobj)
        pause(0.5);
        stop(recobj);
        disp('Recording complete')
        rawrxsignal  = getaudiodata(recobj,'int16');
        rxsignal     = double(rawrxsignal(1:end))/double(intmax('int16')) ;
        
    elseif strcmp(conf.audiosystem,'bypass')
        rawrxsignal = rawtxsignal(:,1);
        rxsignal    = rawrxsignal;
    end
    
    % Plot received signal for debugging
    figure(4);
    plot(txsignal);
    title('Received Signal')
    
    % End
    % Audio Transmission   
    % % % % % % % % % % % %
    
    % TODO: Implement rx() Receive Function
    if strcmp(conf.training,'one')
        [rxbits] = rx(rxsignal,conf);
        if mod(l_tx/2,conf.nsubfreq)~=0
            rxbits_i=rxbits(1:end-nb_zero);
        end
    elseif strcmp(conf.training,'PT')
        [rxbits] = rx_PT(rxsignal,conf);
        % Add zero for last symbol OFDM
        rxbits=[rxbits ; zeros(length(txbits)-length(rxbits),1)];
        l_rx=length(rxbits);
        if mod(l_tx/2,conf.nsubfreq)~=0
            nb_zero_rx=l_rx-l_tx;
            rxbits_i=rxbits(1:end-nb_zero_rx);
        end
    end
    
    if strcmp(conf.mode,'image')
        image_decoder(rxbits_i,image_size) ;
    end
    res.rxnbits(k)      = length(rxbits);  
    res.biterrors(k)    = sum(rxbits ~= txbits);
    
end

per = sum(res.biterrors > 0)/conf.nframes
ber = sum(res.biterrors)/sum(res.rxnbits)
