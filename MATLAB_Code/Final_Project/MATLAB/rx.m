function [rxbits] = rx(rxsignal,conf,k)
% Digital Receiver
%
%   [rxsignal conf] = rx(txbits,conf,k) implements a complete causal
%   receiver in digital domain.
%
%   rxsignal    : received signal
%   conf        : configuration structure
%   k           : frame index
%
%   Outputs
%
%   rxbits      : received bits
%   conf        : configuration structure
%

% LP down conversion 

% f=conf.f_s*(0:length(rxsignal)-1)/length(rxsignal);

time = 0:1/conf.f_s:length(rxsignal)*(1/conf.f_s)-(1/conf.f_s);
LP_down_signal = 2*lowpass(transpose(rxsignal).*exp(-2i*pi*conf.f_c.*time),conf);
% 
f=conf.f_s*(0:length(LP_down_signal)-1)/length(LP_down_signal);
% 
figure(5)
plot(f,fft(LP_down_signal));
title("Signal received in frequency domain");
% xlim([0 1000]);
% ylim([-2 2].*1e4);
xlabel("Frequency(Hz)");
ylabel("FFT Amplitude");
grid on;

    
[data_idx, theta] = frame_sync(matched_filter(LP_down_signal,conf.npreamble, conf.filter_length),conf.os_factor_preamble);

%data_idx = 48000;

length_payload = (conf.nsubfreq*conf.os_factor+conf.cp_length)+(conf.nsubfreq*conf.os_factor+conf.cp_length)*(conf.nsyms/conf.nsubfreq);
rx_payload = LP_down_signal(data_idx:data_idx+length_payload-1);
N=conf.nsubfreq*conf.os_factor+conf.cp_length;   % number of symbol sent on each subcarrier

%Serial to parallel change
S2P_rxsignal = reshape(rx_payload,N,[]) ;


% CP prefixe
S2P_no_cp = S2P_rxsignal(conf.cp_length+1:end,:);

% fft and downsampling
S2P_fft=zeros(conf.nsubfreq,(conf.nsyms/conf.nsubfreq)+1); % Take new oversampling size into account and training symbol
for i =1:(conf.nsyms/conf.nsubfreq)+1
    S2P_fft(:,i) = osfft(S2P_no_cp(:,i),conf.os_factor) ;
end

% Training symbol generation and comparaison

training_symbol_rx = 1-2*lfsr_framesync(conf.nsubfreq);

H_f=S2P_fft(:,1)./training_symbol_rx;

f_6=1:conf.f_spacing:conf.f_spacing*conf.nsubfreq;

figure(6)
plot(f_6,abs(H_f));
% xlim([0 256]);
ylim([0 10]);
title('Channel estimator magnitude in frequency domain');
xlabel('Frequency');
ylabel('$\hat{H}(f)$ Magnitude','interpreter','latex');
grid on;

figure(7)
plot(f_6,angle(H_f));
title('Channel estimator angle in frequency domain');
xlabel('Frequency');
ylabel('$\hat{H}(f))$ Angle','interpreter','latex');
grid on;

%Channel impulse response
h=ifft(H_f);

%Power Delay profile
PDP = abs(h).^2;

figure(8)
plot(PDP);
title('Power Delay Profile');
xlabel('Time axis');
ylabel('PDP');
grid on;

% Equalization

 for i =2:(conf.nsyms/conf.nsubfreq)+1
     S2P_fft(:,i) = (S2P_fft(:,i).*exp(-1i*angle(H_f)))./abs(H_f);
 end

S2P_fft=S2P_fft(:,2:end); %remove training symbol;

%Parralel to serie change
rxsignal = transpose(S2P_fft(:));


figure(9);
plot(real(rxsignal),imag(rxsignal),'r*');
title('Modulated OFDM received signal extracted');
xlabel('Real axis')
ylabel('Imaginary axis')
grid on;

rxbits=demapper(rxsignal);


  



