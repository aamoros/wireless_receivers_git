function [txsignal] = tx(txbits,conf)
% Digital Transmitter % 
% [txsignal conf] = tx(txbits,conf,k) implements a complete transmitter % consisting of: 
%               - modulator 
%               - Serial to Parallel
%               - IFFT and oversampling
%               - Add CP
%               - Parallel to Serial 
%               - preamble and shaping filter on it
%               - mixer around fc
% in digital domain. 
% % txbits : Information bits 
% conf : Universal configuration structure 
% k : Frame index % 

% Training OFDM symbol bits 

training_bits=1 -2*lfsr_framesync(conf.nsubfreq);

%Modulation 
if conf.modulation_order == 1
        GrayMap = 1/sqrt(2) * [-1, 1] ;
else if conf.modulation_order ==2 
    % Gray mapping (Symbols, normalized)
        GrayMap = 1/sqrt(2) * [(-1-1j) (-1+1j) ( 1-1j) ( 1+1j)];
    end
end
txbits=reshape(txbits,2,[]).';
%training_bits=reshape(training_bits,2,[]).';

MappedSignal = GrayMap(bi2de(txbits) +1 );

MappedTraining = training_bits;

MappedWholeSignal =[MappedTraining.'  MappedSignal];

figure(1)
plot(real(MappedWholeSignal),imag(MappedWholeSignal),'*r');
title('Modulated OFDM payload (training + data)');
xlabel('Real axis')
ylabel('Imaginary axis')
grid on;

%Serial to parallel change
num_cols=length(MappedWholeSignal)/conf.nsubfreq;
S2P_signal = reshape(MappedWholeSignal,conf.nsubfreq,[]);

% Ifft and oversampling
S2P_ifft=zeros(conf.nsubfreq*conf.os_factor,num_cols); % Take new oversampling size into account 

for i =1:num_cols
    S2P_ifft(:,i) = osifft(S2P_signal(:,i),conf.os_factor) ;
end

% CP prefixe
S2P_cp_signal = [S2P_ifft(end-conf.cp_length+1:end,:);S2P_ifft];


%Parralel to serie change
cp_signal = transpose(S2P_cp_signal(:));
cp_signal = 30*cp_signal/(norm(cp_signal));% signal normalization

% f_1=conf.f_s*(0:length(cp_signal)-1)/length(cp_signal);
% 
% figure(2)
% plot(f_1,fft(cp_signal));
% title('OFDM signal with CP in frequency domain');
% xlabel('Frequency');
% ylabel('FFT Magnitude');
% grid on;

% Additional preambule

length_preamble=conf.npreamble;
preamble = zeros(length_preamble, 1);
preamble = 1 - 2*lfsr_framesync(length_preamble);

preamble=resample(preamble,conf.f_s,conf.npreamble); % oversampling at the same frequency than the payload
preamble_shaping=transpose(matched_filter(preamble, conf.os_factor_preamble, conf.filter_length)) ; % shaping the preamble for frame sync
preamble_shaping=preamble_shaping/(norm(preamble_shaping)); % normalization of preamble


txsignal =[preamble_shaping  cp_signal]; % add preamble

f_2=conf.f_s*(0:length(txsignal)-1)/length(txsignal);

figure(2)
plot(f_2,fft(txsignal));
title('OFDM signal with preamble in frequency domain');
xlabel('Frequency');
ylabel('FFT Magnitude');
grid on;

% Creation of a frequency vector for the mixer around frequence carrier of 8kHz // Up Conversion
time = 0:1/conf.f_s:length(txsignal)*(1/conf.f_s)-(1/conf.f_s);
payload_signal = real(txsignal.*exp(2i*pi*conf.f_c.*time));


f_3=conf.f_s*(0:length(payload_signal)-1)/length(payload_signal);


figure(3);
%plot(f,10*log10(abs(fft(payload_signal))));
plot(f_3,fft(payload_signal));
title('OFDM spectrum at the transmission side');
xlabel('frequency')
ylabel('FFT Magnitude')
grid on;

txsignal = 50*transpose(payload_signal);

end


