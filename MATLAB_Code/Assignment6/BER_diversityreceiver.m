clear all;

% Oversampling factor
os_factor = 4;

% SNR
SNR = 6;
noAntenna = 3;

% transmitter
load pn_sequence_fading.mat ;
load ber_pn_seq.mat ;

data_length = length(ber_pn_seq)/2;
noframes = 1; 
symbolsperframe = data_length/noframes;

symbolsperframe = data_length/noframes;
rxsymbols = zeros(noframes,symbolsperframe)';


 % Apply Rayleigh Fading Channel
 h = randn(noAntenna,1)+1i*randn(noAntenna,1);
 chanFrame = h * signal;
    
 % Add White Noise
 SNRlin = 10^(SNR/10);
 noiseFrame = chanFrame + 1/sqrt(2*SNRlin)*(randn(size(chanFrame)) + 1i*randn(size(chanFrame)));
 
 for i=1:noAntenna
     
    % Matched Filter
    filtered_rx_signal(i,:) = matched_filter(noiseFrame(i,:), os_factor, 6); % 6 is a good value for the one-sided RRC length (i.e. the filter has 13 taps in total)

    % Frame synchronization
    [data_idx(i) theta(i) magnitude(i)] = frame_sync(filtered_rx_signal(i,:).', os_factor); % Index of the first data symbol
    
 end
 
    %Antenna selection diversity
    
    [maximum k]=max(h);
    
    correct_samples = filtered_rx_signal(k,data_idx(k):os_factor:data_idx(k)+(symbolsperframe*os_factor)-1);
    rxsymbols = magnitude(k)*exp(-1j*theta(k)) *correct_samples ;  
    
    %Antenna selection Maximum Ratio Combining
<<<<<<< HEAD
    
    
    
    correct_samples2 = correct_samples + conj(h(i)) * filtered_rx_signal(i,data_idx(i):os_factor:data_idx(i)+(symbolsperframe*os_factor)-1);
    
    
    
 end
=======
    correct_samples2= zeros(noAntenna, data_length);
    for k=1:noAntenna
        correct_samples2(k,:) = magnitude(k)*exp(-j*theta(k))* filtered_rx_signal(k,data_idx(k):os_factor:data_idx(k)+(symbolsperframe*os_factor)-1);
    end
    rxsymbols2 = sum(correct_samples2)/norm(magnitude.*exp(-j*theta)) ;      
>>>>>>> 734858ecf6dbffe267d17c37bfcafab9b50d6a6b

rxsymbols2 = correct_samples2/norm(h) ;  

combined_rxsymbols =  reshape(rxsymbols.',1,noframes*symbolsperframe);
rxbitstream = demapper(combined_rxsymbols); % Demap Symbols

combined_rxsymbols2 =  reshape(rxsymbols2.',1,noframes*symbolsperframe);
rxbitstream2 = demapper(combined_rxsymbols2); % Demap Symbols

%BER calculation

BER=sum(rxbitstream~=ber_pn_seq)/length(ber_pn_seq);

BER2=sum(rxbitstream2~=ber_pn_seq)/length(ber_pn_seq);