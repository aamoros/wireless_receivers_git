clear all ;

% Oversampling factor
os_factor = 4;

% SNR
SNR = 6;
noAntenna = 3;

% transmitter
load task6_1


data_length = prod(image_size) * 8 / 2; % Number of QPSK data symbols
noframes = size(signal,1); 

symbolsperframe = data_length/noframes;
rxsymbols = zeros(noframes,symbolsperframe);



% Loop through all frames
for k=1:noframes
   
    Frame = signal(k,:);
    
    % Apply Rayleigh Fading Channel
    h = randn(noAntenna,1)+1i*randn(noAntenna,1);
    chanFrame = h * Frame;
    
    % Add White Noise
    SNRlin = 10^(SNR/10);
    noiseFrame = chanFrame + 1/sqrt(2*SNRlin)*(randn(size(chanFrame)) + 1i*randn(size(chanFrame)));
    
    % Receiver with Single Antenna
    %
    
    for i=1:noAntenna
        % Matched Filter
        filtered_rx_signal(i,:) = matched_filter(noiseFrame(i,:), os_factor, 6); % 6 is a good value for the one-sided RRC length (i.e. the filter has 13 taps in total)

        % Frame synchronization
        [data_idx(i) theta(i) magnitude(i)] = frame_sync(filtered_rx_signal(i,:).', os_factor); % Index of the first data symbol

    end
  
    
    
    % Pick correct sampling points of the 1st antenna only
    
    correct_samples1 = filtered_rx_signal(1,data_idx(1):os_factor:data_idx(1)+(symbolsperframe*os_factor)-1);
    
    [rxsymbols1(k,:)] = magnitude(1)*exp(-1j*theta(1)) *correct_samples1 ; 
    
    %Antenna selection diversity
    
    [maximum i]=max(h);
    
    correct_samples = filtered_rx_signal(i,data_idx(i):os_factor:data_idx(i)+(symbolsperframe*os_factor)-1);
    [rxsymbols(k,:)] = magnitude(i)*exp(-1j*theta(i)) *correct_samples ;  
    
end

combined_rxsymbols1 = reshape(rxsymbols1.',1,noframes*symbolsperframe);
rxbitstream1 = demapper(combined_rxsymbols1); % Demap Symbols
image_decoder(rxbitstream1,image_size) % Decode Image

combined_rxsymbols = reshape(rxsymbols.',1,noframes*symbolsperframe);
rxbitstream = demapper(combined_rxsymbols); % Demap Symbols
image_decoder(rxbitstream,image_size) % Decode Image

