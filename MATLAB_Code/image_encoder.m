function [bits, size_vector] = image_encoder(path_photo)

I = imread(path_photo);
I = I(:,:,1);
size_vector = size(I);

im_bin = dec2bin(I(:));
im_bin = im_bin(:);
bits = str2num(im_bin);

return