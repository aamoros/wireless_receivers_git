function task2_script
% ex3_3_2 - Task 3.3.2
%    Implement a complet baseband transimission system to measure the BER
%    
% 
% Author(s): Nicholas Preyss
% Copyright (c) 2012 TCL.
% 

%%generation of random bitstram
N=1000;
os_factor = 4;
rand_bitstream_ref=randi([0 1],N,1);
rand_bitstream=reshape(rand_bitstream_ref,2,N/2);


%%Convert to QPSK symbols
index_bits_pair_01=(rand_bitstream(1,:)==0 & rand_bitstream(2,:)==1);
index_bits_pair_10=(rand_bitstream(1,:)==1 & rand_bitstream(2,:)==0);
index_bits_pair_11=(rand_bitstream(1,:)==1 & rand_bitstream(2,:)==1);
index_bits_pair_00=(rand_bitstream(1,:)==0 & rand_bitstream(2,:)==0);

QPSK_symbols = (1/sqrt(2)) * ( (-1+i).* index_bits_pair_01 + (1-i).* index_bits_pair_10 + (1+i).* index_bits_pair_11 + (-1-i).* index_bits_pair_00 );
QPSK_symbols = reshape(QPSK_symbols,N/2,1);

%%Upsampling by factor 4
QPSK_symbols = upsample(QPSK_symbols, os_factor);

%%Shape RRC pulse
filter_length_RRC = (41-1)/2;
roll_off_factor = 0.22;
RRC_pulse = rrc(os_factor, roll_off_factor, filter_length_RRC);

QPSK_symbols_shaped=conv(QPSK_symbols, RRC_pulse,'same');

%%Add AWGN noise 
SNR = 8;
SNRlin = 10^(SNR/10);
QPSK_symbols_shaped_noisy = QPSK_symbols_shaped + sqrt(1/(2*SNRlin)) * (randn(size(QPSK_symbols_shaped)) + 1i*randn(size(QPSK_symbols_shaped)) );

%%Apply matched filter
filter_length_MF = 30;
coeff_filter = rrc(os_factor, roll_off_factor, filter_length_MF);
filtered_QPSK_symbols=conv(QPSK_symbols_shaped_noisy, coeff_filter, 'same');

%%Revert upsampling
filtered_QPSK_symbols=downsample(filtered_QPSK_symbols,4);

%%Demap the symbol stream
demapping_noisy_signal = demapper(filtered_QPSK_symbols);

%%Calcul the BER
BER = mean(rand_bitstream_ref ~= demapping_noisy_signal);


BER< 7e-3 %the minimal length is 31 as filter_length 


