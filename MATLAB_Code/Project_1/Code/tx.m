function [txsignal conf] = tx(txbits,conf,k)
% Digital Transmitter % 
% [txsignal conf] = tx(txbits,conf,k) implements a complete transmitter % consisting of: 
%               - modulator 
%               - pulse shaping filter 
%               - up converter 
% in digital domain. 
% % txbits : Information bits 
% conf : Universal configuration structure 
% k : Frame index % 

%Modulation 
if conf.modulation_order == 1
        GrayMap = 1/sqrt(2) * [-1, 1] ;
else if conf.modulation_order ==2 
    % Gray mapping (Symbols, normalized)
        GrayMap = 1/sqrt(2) * [(-1-1j) (-1+1j) ( 1-1j) ( 1+1j)];
    end
end
txbits=reshape(txbits,2,conf.nsyms).';
MappedSignal = GrayMap(bi2de(txbits) +1 );

% Additional preambule

length_preamble=conf.npreamble;
preamble = zeros(length_preamble, 1);
preamble = 1 - 2*lfsr_framesync(length_preamble);

whole_signal = [preamble' MappedSignal];

figure(1)
plot(real(whole_signal),imag(whole_signal),'r*');

whole_signal =  resample(whole_signal,conf.f_s,conf.f_sym) ;

%Pulse shaping filter 
txsignal_filtered = matched_filter(whole_signal, conf.os_factor, conf.filter_length) ;

% figure(2)
% plot(real(txsignal_filtered),imag(txsignal_filtered),'r*');
% title("Signal right after pulse shaping");

f=conf.f_s*(0:length(txsignal_filtered)-1)/length(txsignal_filtered);

figure(2)
plot(f,fft(txsignal_filtered));
title("Signal right after pulse shaping in frequency domain [0 5kHz]");
xlim([0 5000]);
ylim([-1.5 1.5].*1e5);
xlabel("Frequency(Hz)");
ylabel("FFT Amplitude");
grid on;

% Up Conversion
time = 0:1/conf.f_s:length(txsignal_filtered)*(1/conf.f_s)-(1/conf.f_s);
up_converted_signal = real(txsignal_filtered.*exp(2i*pi*conf.f_c*time));

figure(3)
plot(f,fft(up_converted_signal));
title("Signal right after up conversion in frequency domain [0 5kHz]");
xlim([0 5000]);
xlabel("Frequency(Hz)");
ylabel("FFT Amplitude");
grid on;

txsignal=up_converted_signal' ;

end


