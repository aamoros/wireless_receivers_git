function [rxbits , conf, theta_hat] = rx(rxsignal,conf,k)
% Digital Receiver
%
%   [rxsignal conf] = rx(txbits,conf,k) implements a complete causal
%   receiver in digital domain.
%
%   rxsignal    : received signal
%   conf        : configuration structure
%   k           : frame index
%
%   Outputs
%
%   rxbits      : received bits
%   conf        : configuration structure
%

% LP down conversion 

f=conf.f_s*(0:length(rxsignal)-1)/length(rxsignal);

figure(4)
plot(f,fft(rxsignal));
title("Signal received in frequency domain [0 5kHz]");
xlim([0 5000]);
ylim([-1.5 1.5].*1e4);
xlabel("Frequency(Hz)");
ylabel("FFT Amplitude");
grid on;

time = 0:1/conf.f_s:length(rxsignal)*(1/conf.f_s)-(1/conf.f_s);
LP_down_signal = 2*lowpass(rxsignal.'.*exp(-2i*pi*conf.f_c*time),conf);

figure(5)
plot(f,fft(LP_down_signal));
title("Signal right after down-conversion in frequency domain [0 5kHz]");
xlim([0 5000]);
ylim([-2 2].*1e4);
xlabel("Frequency(Hz)");
ylabel("FFT Amplitude");
grid on;

% Matched filter
filtered_rx_signal = matched_filter(LP_down_signal,conf.os_factor, conf.filter_length) ;

% Frame synchronization
[data_idx, theta] = frame_sync(filtered_rx_signal,conf.os_factor) ;

data_length = conf.nsyms; % Number of QPSK data symbols
data = zeros(1,data_length);

% Timing estimator initialization
epsilon  = zeros(1,data_length); % Estimate of the timing error
diff_err = zeros(1,data_length);
cum_err = 0;

% Phase estimator initialization
theta_hat = zeros(1,data_length+1);   % Estimate of the carrier phase
theta_hat(1) = theta;

for i=1:data_length
    
     idx_start  = data_idx+(i-1)*conf.os_factor;  
     idx_range  = idx_start:idx_start+conf.os_factor-1;
     segment    = filtered_rx_signal(idx_range);

     % Timing estimator
     pwr         = abs(segment).^2;
     diff_err(i) = exp(-2*pi*j*(0:1/480:1-1/480))*pwr';
     cum_err     = cum_err + diff_err(i);
     epsilon(i)  = -1/(2*pi)*angle(cum_err);     
     
     % Interpolate to choose best sampling point
     sample_diff   = floor(epsilon(i)*conf.os_factor); % integer
     int_diff      = mod(epsilon(i)*conf.os_factor,1); % decimal part interval [0 1)
    
     
      % linear
%     y     = filtered_rx_signal(idx_start+sample_diff:idx_start+sample_diff+1);
%     y_hat = y(1)+int_diff*(y(2)-y(1));
%     data(i) = y_hat;
%     data_n(i)=data(i);

     % cubic
      y     = filtered_rx_signal(idx_start+sample_diff-1:idx_start+sample_diff+2).';
      c = 1/6 * [-1 3 -3 1 ; 3 -6 3 0 ; -2 -3 6 -1 ; 0 6 0 0] * y;
      y_hat = c(1) * int_diff^3 + c(2) * int_diff^2 + c(3) * int_diff + c(4);
      data(i) = y_hat; 

     % Phase estimator
     % Apply viterbi-viterbi algorithm
     deltaTheta = 1/4*angle(-data(i)^4) + pi/2*(-1:4);
     
     [~, ind] = min(abs(deltaTheta - theta_hat(i)));
     theta = deltaTheta(ind);
     
     % Lowpass filter phase
     theta_hat(i+1) = mod(0.01*theta + 0.99*theta_hat(i), 2*pi);
    
     % Phase correction
     data(i) = data(i) * exp(-1j * theta_hat(i+1)); % rotate signal accordingly to phase offset

end

figure(6)
plot(real(data),imag(data),'b*');
title("Signal before demapping");

rxbits=demapper(data);
