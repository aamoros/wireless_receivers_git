%%% AWGN Channel simulation

tic();

clc;


%% Load image payload data

image_data = load("image.mat");

signal = image_data.signal;

image_size = image_data.image_size ;

%% Generate White noise signal

SNR = 1;

sigma = exp(-SNR/10);

w = normrnd(0,(sigma.^2/2),length(signal),2);

%% Simulate received signal with channel's impact

r = signal + w(:,1) + 1i.*w(:,2); 

%% Demaping signal 

bits_estimated = zeros(2*length(signal),1);

index_bits_pair_01=(angle(r)>0 & angle(r)<=pi/2);
index_bits_pair_10=angle(r)>pi/2 & angle(r)<=pi;
index_bits_pair_11=angle(r)>pi & angle(r)<=3*pi/2;
index_bits_pair_00=angle(r)>3*pi/2 & angle(r)<=2*pi;

matrix_QSPK_01 = reshape(index_bits_pair_01'.*[0;1],2*length(signal),1); 
matrix_QSPK_10 = reshape(index_bits_pair_10'.*[1;0],2*length(signal),1);
matrix_QSPK_11 = reshape(index_bits_pair_11'.*[1;1],2*length(signal),1);
matrix_QSPK_00 = reshape(index_bits_pair_00'.*[0;0],2*length(signal),1);

bits_estimated = matrix_QSPK_01 + matrix_QSPK_10 + matrix_QSPK_11 + matrix_QSPK_00 ;

%% Image display

% image_decoder(bits_estimated, image_size);

%% Plot received symbols

normalize_symbols = 1/sqrt(2) .* [ 1+i ; 1-i; -1-i ; -1+i ];

figure(2);
hold on
plot(real(r),imag(r),'r*');
plot(real(normalize_symbols),imag(normalize_symbols),'bo');
title('Received symbol constellation')
legend('Received symbols with AWGN channel','Expected symbol value')
grid on;
hold off;

%% BER simulation mapping 1

N = 100000;  % N must be pair
random_seq1=randi([0 1],N,1);
random_seq=reshape(random_seq1,2,N/2);

index_bits_pair_01=(random_seq(1,:)==0 & random_seq(2,:)==1);
index_bits_pair_10=(random_seq(1,:)==1 & random_seq(2,:)==0);
index_bits_pair_11=(random_seq(1,:)==1 & random_seq(2,:)==1);
index_bits_pair_00=(random_seq(1,:)==0 & random_seq(2,:)==0);

mapped_signal_1 = (1/sqrt(2)) * ( (-1+i).* index_bits_pair_01 + (1-i).* index_bits_pair_10 + (1+i).* index_bits_pair_11 + (-1-i).* index_bits_pair_00 );
mapped_signal_1 = reshape(mapped_signal_1,N/2,1);

SNR = [-6:0.5:12];
BER_1 = zeros(length(SNR),1);

for k=1:length(SNR)
    
sigma_r= exp(-SNR(k)/10);
w_r = normrnd(0,(sigma_r.^2/2),N/2,2);

r_random = mapped_signal_1 + w_r(:,1) + 1i.*w_r(:,2); 

bits_pair_11=(angle(r_random)>0 & angle(r_random)<=pi/2 );
bits_pair_01=(angle(r_random)>pi/2 & angle(r_random)<=pi);
bits_pair_00=(angle(r_random)>-pi & angle(r_random)<=-pi/2);
bits_pair_10=(angle(r_random)>-pi/2 & angle(r_random)<=0);

matrix_01 = reshape(bits_pair_01'.*[0;1],N,1); 
matrix_10 = reshape(bits_pair_10'.*[1;0],N,1);
matrix_11 = reshape(bits_pair_11'.*[1;1],N,1);
matrix_00 = reshape(bits_pair_00'.*[0;0],N,1);

bits_estimated_random = matrix_01 + matrix_10 + matrix_11 + matrix_00 ;

errors = sum(bits_estimated_random~=random_seq1);

BER_1(k)=errors/N;


end;


%% BER simulation mapping 2

mapped_signal_2 = (1/sqrt(2)) * ( (1+i).* index_bits_pair_01 + (-1+i).* index_bits_pair_10 + (-1-i).* index_bits_pair_11 + (1-i).* index_bits_pair_00 );
mapped_signal_2 = reshape(mapped_signal_2,N/2,1);

BER_2 = zeros(length(SNR),1);

for k=1:length(SNR)
    
sigma_r= exp(-SNR(k)/10);
w_r = normrnd(0,(sigma_r.^2/2),N/2,2);

r_random = mapped_signal_2 + w_r(:,1) + 1i.*w_r(:,2); 

bits_pair_01=(angle(r_random)>0 & angle(r_random)<=pi/2 );
bits_pair_10=(angle(r_random)>pi/2 & angle(r_random)<=pi);
bits_pair_11=(angle(r_random)>-pi & angle(r_random)<=-pi/2);
bits_pair_00=(angle(r_random)>-pi/2 & angle(r_random)<=0);

matrix_01 = reshape(bits_pair_01'.*[0;1],N,1); 
matrix_10 = reshape(bits_pair_10'.*[1;0],N,1);
matrix_11 = reshape(bits_pair_11'.*[1;1],N,1);
matrix_00 = reshape(bits_pair_00'.*[0;0],N,1);

bits_estimated_random = matrix_01 + matrix_10 + matrix_11 + matrix_00 ;

errors = sum(bits_estimated_random~=random_seq1);

BER_2(k)=errors/N;

end;

%% Plot BER simulation

figure(3);
semilogy(SNR,BER_1);
hold on;
semilogy(SNR,BER_2);
hold off;
title("BER in function of SNR with AWGN Channel")
xlabel("SNR")
ylabel("BER")
legend("Mapping 1","Mapping 2")
grid on;

%We can note that the first mapping is better than the second because when
%there is a decoding error it's only 1 bit wrong with mapping 1 rather 2
%wrong with mapping 2
%% Compressed decoder call

compressed_decoder(bits_estimated, image_size);

%% Stop measurement
toc()