% Theory task assignment 1 - Energy normalization

clear all;
clc;

blue_const = [3i -3i 3 -3 6+6i -6-6i ];
red_const = [ 3+3i 3-3i -3-3i -3+3i];

E_blue=(1/length(blue_const)).*sum(norm(blue_const).^2);
E_red=(1/length(red_const)).*sum(norm(red_const).^2);

norm_blue = (1/sqrt(E_blue))*blue_const;
norm_red = (1/sqrt(E_red))*red_const;


E_norm_blue=(1/length(norm_blue)).*sum(abs(norm_blue).^2);
E_norm_red=(1/length(norm_red)).*sum(abs(norm_red).^2);

figure(1);
hold on;
plot(real(norm_blue),imag(norm_blue),'b*');
plot(real(norm_red),imag(norm_red),'r*');
grid on;

% We can observe that by normalizing, the blue constellation seems to be closer
% of center point than the red constellation compared to no-normalized
% constellation. A no-symetric constellation ensuing a bad dimension ratio after normalization.
% This way would be easier to make mistake bit error during demodulation
%