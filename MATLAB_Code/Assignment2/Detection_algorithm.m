%% Initialization
clear all
close all
profile on

%% Initialization of the preambule and conversion to QPSK

Np = 100;
initial_state = ones(8,1);

preambule_bin = LFSR(Np, initial_state);
preambule_BPSK = 1*(preambule_bin == 1) + -1*(preambule_bin==0);
%% Importation of data and adding noise

task2 = load('task2.mat');
signal = task2.signal;
SNR = 20;

sigma = exp(-SNR/10);

w = normrnd(0,(sigma.^2/2),length(signal),2);
r = signal + w(:,1) + 1*i.*w(:,2); 

%% Correlation
%corr1 = xcorr(r,preambule_BPSK);

% cela semble correct tout de meme
corr = zeros(length(signal),1);
for n=1:1:length(signal)
    for i=0:1:Np-1
        if (n-i) < 1
            fact = 0;
        else 
            fact = r(n-i);
        end
        corr(n)=corr(n)+preambule_BPSK(Np-i)*fact;
    end
end

% figure(1)
% plot(abs(corr))

%% Peak threshold



gamma = zeros(length(signal),1);

for n=1:1:length(signal)
    for i=0:1:Np-1
        if (n-i) < 1
            fact = 0;
        else 
            fact = r(n-i);
        end
        gamma(n)=abs(corr(n))^2/(gamma(n)+abs(fact)^2);
    end
end

figure(2)
plot(gamma);

%% Find start signal

threshold = 35;
gamma=gamma>threshold;

index_start=find(gamma==1)+1;

signal_received=r(index_start(1):end);

bits_received = demapper(signal_received);


image_decoder(bits_received(1:8*prod(task2.image_size)),task2.image_size);


profile off
profile viewer