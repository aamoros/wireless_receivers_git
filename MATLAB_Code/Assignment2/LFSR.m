function preambule = LFSR(Np, initial_state)
% LFSR - compute a Linear Feedback Shift Register at 
% a given fixed size and return the preambule .
%    LFSR(Np, inital_state)
%    
%    Arguments:
%      Np:            (integer) length of the preambule
%      Initial_state: (vector) initialisation of the LSFR, bit vector of
%                       length 8
% 
% Author(s): unknown
% Copyright (c) 2011 RWTH.

% Compute a LFSR in order to have a preambule, and return it.

preambule=zeros(Np,1);
l_LFSR = initial_state;

for i = 1:Np
    preambule(i)= l_LFSR(8);
    sum1        = xor(l_LFSR(8), l_LFSR(6));
    sum2        = xor(sum1, l_LFSR(5));
    sum3        = xor(sum2, l_LFSR(4));
    l_LFSR(2:8) = l_LFSR(1:7);
    l_LFSR(1)   = sum3;
end   
    
end    