clc;
clear all;

os_factor = 4;
SNR = 10;

load ./task4

data_length = prod(image_size) * 8 / 2; % Number of QPSK data symbols

% convert SNR from dB to linear
SNRlin = 10^(SNR/10);

% add awgn channel
rx_signal = signal + sqrt(1/(2*SNRlin)) * (randn(size(signal)) + 1i*randn(size(signal)) ); 

% Matched filter
filtered_rx_signal = matched_filter(rx_signal, os_factor, 6); % 6 is a good value for the one-sided RRC length (i.e. the filter has 13 taps in total)

% Frame synchronization
data_idx = frame_sync(filtered_rx_signal, os_factor); % Index of the first data symbol

data = zeros(1,data_length);
data_cubic = zeros(1,data_length);

cum_err = 0;
diff_err = zeros(1,data_length);
epsilon  = zeros(1,data_length);
sum_arg=0;

%%
for i=1:data_length
    
     idx_start  = data_idx+(i-1)*os_factor;
     idx_range  = idx_start:idx_start+os_factor-1;
     segment    = filtered_rx_signal(idx_range);
    
     % Estimate timing error epsilon
      instant_signal_power=abs(segment).^2;
      
     
      for k=1:length(segment)
          sum_arg=sum_arg+instant_signal_power(k)*(-1j)^(k-1);
      end
      
      epsilon(i)=(1/(2*pi))*(angle(sum_arg)+pi);
      
     % Chose best sampling point
     
     
     if (epsilon(i)>0 && epsilon(i)<(1/4))
        best_sampling_point=segment(1:2);
        other_point=[filtered_rx_signal(idx_start-1) segment(3)];
     elseif (epsilon(i)>(1/4) && epsilon(i)<(2/4))
        best_sampling_point=segment(2:3);
        other_point=[segment(1) segment(4)];
     elseif (epsilon(i)>(2/4) && epsilon(i)<(3/4))
        best_sampling_point=segment(3:4);
        other_point=[segment(2) filtered_rx_signal(idx_start+os_factor)];
     elseif (epsilon(i)>(3/4) && epsilon(i)<1)
        best_sampling_point=[segment(4) filtered_rx_signal(idx_start+os_factor)];
        other_point=[segment(3) filtered_rx_signal(idx_start+os_factor+1)];
     end
        
%      if (epsilon(i)>0 && epsilon(i)<(1/3))
%         best_sampling_point=segment(1:2);
%         other_point=[segment(4) segment(3)];
%      elseif (epsilon(i)>(1/3) && epsilon(i)<(2/3))
%         best_sampling_point=segment(2:3);
%         other_point=[segment(1) segment(4)];
%      elseif (epsilon(i)>(2/3) && epsilon(i)<1)
%         best_sampling_point=segment(3:4);
%         other_point=[segment(2) filtered_rx_signal(idx_start+os_factor)];
%      end
     
     
     % Interpolate linear
 
      data(i) = (1-epsilon(i))*best_sampling_point(1)+epsilon(i)*best_sampling_point(2);
      
      % Cubic interpolator
      
      prod_matrix=[-1 3 -3 1 ; 3 -6 3 0; -2 -3 6 -1; 0 6 0 0];
      coeff_matrix=(1/6)*prod_matrix*[other_point(1);best_sampling_point(1);best_sampling_point(2);other_point(2)];
      
      data_cubic(i)=coeff_matrix(1)*(epsilon(i)^3)+coeff_matrix(2)*(epsilon(i)^2)+coeff_matrix(3)*(epsilon(i))+coeff_matrix(4);
      
end

image_decoder(demapper(data), image_size);

image_decoder(demapper(data_cubic), image_size);

%% 
